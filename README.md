# vmxNoPromisc

This program will traverse a given root directory and subdirectories to it, looking for .vmx files.
It will then open each of those files, detecting the number of ethernet adapters (by finding ethernetX.present
 variables).
Further to that, it will also look for existing ethernetX.noPromisc values and skip these interfaces.

Any interfaces without a noPromisc will be given a ethernetX.noPromisc = "TRUE".
Those already set are ignored, so if manually overridden with "FALSE" then it won't be conflicting.

Note: X in ethernetX refers to the corresponding ethernet adapter ID given by VMWare Workstation.

Tested on VMWare Workstation 16 on Windows 11 22H2 build.