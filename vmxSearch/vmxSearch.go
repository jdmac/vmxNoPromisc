package vmxSearch

import (
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
)

func Search(directory string) ([]string, error) {
	var paths []string

	// Check if directory path exists
	info, err := os.Stat(directory)
	if errors.Is(err, os.ErrNotExist) {
		return paths, errors.New(fmt.Sprintf("Provided path, %v, could not be found", directory))
	}

	if !info.IsDir() {
		return paths, errors.New(fmt.Sprintf("%v is not a directory", directory))
	}

	err = filepath.WalkDir(directory, func(path string, info fs.DirEntry, err error) error {
		if err != nil {
			fmt.Printf("prevent panic by handling failure accessing a path %q: %v\n", path, err)
			return err
		}
		if filepath.Ext(path) == ".vmx" && !info.IsDir() {
			paths = append(paths, path)
		}
		return nil
	})

	if err != nil {
		return paths, err
	}

	return paths, nil
}
