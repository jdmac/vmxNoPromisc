//go:build windows

package main

import (
	"fmt"
	"log"
	"os"
	"vmxNoPromisc/vmxEditor"
	"vmxNoPromisc/vmxSearch"
)

func main() {
	var directory string
	var err error
	if len(os.Args) > 1 {
		directory = os.Args[1]
	} else {
		directory, err = os.Getwd()
		if err != nil {
			log.Println(err)
			os.Exit(1)
		}
	}

	log.Printf("Working directory: %v", directory)
	vmxFiles, err := vmxSearch.Search(directory)
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}

	for _, vmxFile := range vmxFiles {
		err = vmxEditor.UpdateVMX(vmxFile)
		if err != nil {
			log.Println(err)
		}
	}

	fmt.Println("Press any key to continue")
	fmt.Scanln()
}
