package vmxEditor

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func UpdateVMX(path string) error {
	file, err := os.OpenFile(path, os.O_APPEND, 0666)
	defer file.Close()
	if err != nil {
		return err
	}

	var interfacesFound []string
	var interfacesSkipped []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if strings.Contains(scanner.Text(), "ethernet") && strings.Contains(scanner.Text(), "present") {
			interfacesFound = append(interfacesFound, strings.Split(scanner.Text(), ".")[0])
		}
		if strings.Contains(scanner.Text(), "ethernet") && strings.Contains(scanner.Text(), "noPromisc") {
			interfacesSkipped = append(interfacesSkipped, strings.Split(scanner.Text(), ".")[0])
		}
	}

	if err = scanner.Err(); err != nil {
		return err
	}

	var appendToFile []string
	for _, foundInterface := range interfacesFound {
		skip := false
		for _, skippedInterface := range interfacesSkipped {
			if foundInterface == skippedInterface {
				skip = true
				break
			}
		}
		if skip == false {
			appendToFile = append(appendToFile, foundInterface)
		}
	}

	if len(appendToFile) > 0 {
		log.Printf("Appending to %v\n", path)
		for _, appendable := range appendToFile {
			log.Printf(fmt.Sprintf("\t%v.noPromisc = \"TRUE\"\n", appendable))
			_, err = file.WriteString(fmt.Sprintf("%v.noPromisc = \"TRUE\"\n", appendable))
			if err != nil {
				return err
			}
		}
	}

	return nil
}
